//
//  LoginViewController.swift
//  MVVM_Sample
//
//  Created by Parth iOS  on 11/07/23.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    var loginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginViewModel.delegate = self
        
    }
    @IBAction func btnLoginAction(_ sender: UIButton) {
        
        let loginRequestModel = LoginRequestModel(userEmail: self.txtUserName.text ?? "", userPassword: self.txtPassword.text ?? "")
        
        loginViewModel.loginUser(loginRequest: loginRequestModel)
    }
}

extension LoginViewController: LoginViewModelDelegate {
    func didReceiveLoginResponse(loginResponse: LoginResponseModel?) {
        
        if(loginResponse?.errorMessage == nil && loginResponse?.data != nil)
        {
            debugPrint("navigate to different view controller")
        }
        else if (loginResponse?.errorMessage != nil)
        {
            let alert = UIAlertController(title: Constants.ErrorAlertTitle, message: loginResponse?.errorMessage, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: Constants.OkAlertTitle, style: .default, handler: nil))

            self.present(alert, animated: true)
        }
    }
}
