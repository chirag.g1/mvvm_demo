//
//  LoginViewModel.swift
//  MVVM_Sample
//
//  Created by Parth iOS  on 11/07/23.
//

import Foundation
protocol LoginViewModelDelegate {
    func didReceiveLoginResponse(loginResponse: LoginResponseModel?)
}
    
struct LoginViewModel
{
    var delegate : LoginViewModelDelegate?

    func loginUser(loginRequest: LoginRequestModel)
    {
        let validationResult = LoginValidation().Validate(loginRequestModel: loginRequest)

        if(validationResult.success)
        {
            
            let httputility = HttpUtility()
            let loginUrl = URL(string: "https://api-dev-scus-demo.azurewebsites.net/api/User/Login")!
            let httpUtility = HttpUtility()
            do {

                let loginPostBody = try JSONEncoder().encode(loginRequest)
                httpUtility.postApiData(requestUrl: loginUrl, requestBody: loginPostBody, resultType: LoginResponseModel.self) { (loginApiResponse) in

                    self.delegate?.didReceiveLoginResponse(loginResponse: loginApiResponse)

                }
            }
            catch let error {
                debugPrint(error)
            }
            
        } else {
            self.delegate?.didReceiveLoginResponse(loginResponse: LoginResponseModel(errorMessage: validationResult.error, data: nil))
        }
    }
}
