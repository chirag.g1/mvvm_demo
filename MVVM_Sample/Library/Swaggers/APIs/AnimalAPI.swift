//
// AnimalAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire


open class AnimalAPI {
    /**

     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiAnimalGetAnimalsGet(completion: @escaping ((_ data: AnimalResponseArrayResponseModel?,_ error: Error?) -> Void)) {
        apiAnimalGetAnimalsGetWithRequestBuilder().execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     - GET /api/Animal/GetAnimals
     - 

     - examples: [{contentType=application/json, example={
  "Data" : [ {
    "image" : "image",
    "name" : "name"
  }, {
    "image" : "image",
    "name" : "name"
  } ],
  "ErrorMessage" : "ErrorMessage"
}}]

     - returns: RequestBuilder<AnimalResponseArrayResponseModel> 
     */
    open class func apiAnimalGetAnimalsGetWithRequestBuilder() -> RequestBuilder<AnimalResponseArrayResponseModel> {
        let path = "/api/Animal/GetAnimals"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        let url = URLComponents(string: URLString)


        let requestBuilder: RequestBuilder<AnimalResponseArrayResponseModel>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
    /**

     - parameter name: (query)  (optional)
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiAnimalSearchAnimalGet(name: String? = nil, completion: @escaping ((_ data: Void?,_ error: Error?) -> Void)) {
        apiAnimalSearchAnimalGetWithRequestBuilder(name: name).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }


    /**
     - GET /api/Animal/SearchAnimal
     - 

     - parameter name: (query)  (optional)

     - returns: RequestBuilder<Void> 
     */
    open class func apiAnimalSearchAnimalGetWithRequestBuilder(name: String? = nil) -> RequestBuilder<Void> {
        let path = "/api/Animal/SearchAnimal"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
                        "name": name
        ])


        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
}
