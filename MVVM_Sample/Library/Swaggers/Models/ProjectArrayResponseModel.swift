//
// ProjectArrayResponseModel.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ProjectArrayResponseModel: Codable {

    public var errorMessage: String?
    public var data: [Project]?

    public init(errorMessage: String? = nil, data: [Project]? = nil) {
        self.errorMessage = errorMessage
        self.data = data
    }

    public enum CodingKeys: String, CodingKey { 
        case errorMessage = "ErrorMessage"
        case data = "Data"
    }

}
