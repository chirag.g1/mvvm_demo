//
// UserResponseModelResponseModel.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct UserResponseModelResponseModel: Codable {

    public var errorMessage: String?
    public var data: UserResponseModel?

    public init(errorMessage: String? = nil, data: UserResponseModel? = nil) {
        self.errorMessage = errorMessage
        self.data = data
    }

    public enum CodingKeys: String, CodingKey { 
        case errorMessage = "ErrorMessage"
        case data = "Data"
    }

}
